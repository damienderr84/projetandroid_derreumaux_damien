package fr.uavignon.ceri.tp3;


import android.app.Activity;
import android.content.Context;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Collectable;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp3.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp3.RecyclerAdapter.class.getSimpleName();
    private ListViewModel listViewModel;

    private static int droite_gauche = 0;
    private ArrayList<Collectable> collectableList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v=null;
        if(droite_gauche % 2 == 0) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }else{
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout2, viewGroup, false);
        }
        droite_gauche++;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(collectableList.get(i).getName());
        viewHolder.itemBrand.setText(collectableList.get(i).getBrand()); //marque de la société ayant produit l'objet

        if (collectableList.get(i).getPicture()!=null){
            Glide.with(viewHolder.itemView.getContext())
                    .load(collectableList.get(i).getPicture())
                    .into(viewHolder.itemImage);
        }

        if (collectableList.get(i).getYear() == 0) {
            viewHolder.itemYear.setText("");
        } else { viewHolder.itemYear.setText(String.valueOf(collectableList.get(i).getYear())); }
    }

    @Override
    public int getItemCount() {
        return collectableList == null ? 0 : collectableList.size();
    }

    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    private void deleteCollectable(String id) {
        if (listViewModel != null)
            listViewModel.deleteCollectable(id);
    }

    public void setCollectableList(ArrayList<Collectable> collectables) {
        collectableList = collectables;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemBrand;
        ImageView itemImage;
        TextView itemYear;

        ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemBrand = itemView.findViewById(R.id.item_detail);
            itemImage = itemView.findViewById(R.id.item_image);
            itemYear = itemView.findViewById(R.id.item_year);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.tp3.RecyclerAdapter.this.deleteCollectable(idSelectedLongClick);
                            Snackbar.make(itemView, "Objet supprimé de la collection !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish(); // Action picked, so close the CAB
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    String id = RecyclerAdapter.this.collectableList.get((int)getAdapterPosition()).getId();
                    ListFragmentDirections  .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    action.setItemNum(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });
        }
    }
}
